# Spring Boot Web whit Spring Security an Thymeleaf Demo

Demo Web App that integrate tree Java technologies:
  -Spring Boot: Embebed tomcat container
  -Spring Security: powerful and highly customizable authentication and access-control framework
  -Thymeleaf: modern server-side Java template engine for both web and standalone environments.

For more reference, see:

https://spring.io/projects/spring-boot
https://spring.io/projects/spring-security
https://www.thymeleaf.org/

This demo is based in:
Web Page: https://www.mkyong.com/spring-boot/spring-boot-spring-security-thymeleaf-example/
Web Page: https://github.com/cruizg93/SpringBoot-Security-MySql/blob/master/src/main/java/com/cristianruizblog/loginSecurity/config/WebSecurityConfig.java
Course: Desarrollo Web con Spring 4 / Udemy

Notes: in the class SpringSecurityConfig we have the method "configureGlobal", in it have two ways for authenticate:

  Using UserDetailService: auth.userDetailsService(customerLoginService).passwordEncoder(getPasswordEncoder())
  Using AuthenticationProvider: auth.authenticationProvider(customAuthenticationProvider)
