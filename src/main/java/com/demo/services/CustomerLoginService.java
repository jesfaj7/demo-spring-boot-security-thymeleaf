package com.demo.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.demo.pojo.UserDTO;
import com.demo.utils.UserFactory;

@Service
public class CustomerLoginService implements UserDetailsService{
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        UserDTO userDTO = getUserInfo(username);
        
        if(null != userDTO){
            authorities.add(new SimpleGrantedAuthority("ROLE_" + userDTO.getRole()));
            authorities.add(new SimpleGrantedAuthority("ROLE_" + userDTO.getRole()));
            User user = new User(userDTO.getUsername(), getEncodePassword(userDTO.getPassword()), authorities);
            return user;
        }else{
            throw new UsernameNotFoundException("Usuario no encontrado");
        }
	}
	
	private String getEncodePassword(String pass) {
		  BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(4);
		  return bCryptPasswordEncoder.encode(pass);
	}
	
	private UserDTO getUserInfo(String username) {		
		if(existUserByUsername(username)) {
			return UserFactory.getUserList().stream().filter(user -> user.getUsername().equals(username)).findFirst().get();
		}
		return null;
	}	

	private boolean existUserByUsername(String username) {
		return UserFactory.getUserList().stream().filter(user -> user.getUsername().equals(username)).findAny().isPresent();
	}
}
