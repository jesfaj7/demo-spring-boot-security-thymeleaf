package com.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DefaultController {

    @GetMapping("/")
    public String landingPage() {
        return "/home";
    }

    @GetMapping("/home")
    public String homePage() {
        return "/home";
    }

    @GetMapping("/admin")
    public String adminPage() {
        return "/admin";
    }

    @GetMapping("/user")
    public String userPage() {
        return "/user";
    }

    @GetMapping("/about")
    public String aboutPage() {
        return "/about";
    }

    @GetMapping("/login")
    public String loginPage() {
        return "/login";
    }

    @GetMapping("/403")
    public String error403Page() {
        return "/error/403";
    }

}
