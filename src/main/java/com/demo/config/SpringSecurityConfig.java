package com.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

// import com.demo.providers.CustomAuthenticationProvider;
import com.demo.services.CustomerLoginService;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AccessDeniedHandler accessDeniedHandler;
    
     @Autowired
     private CustomerLoginService customerLoginService; 

    // @Autowired
    // private CustomAuthenticationProvider customAuthenticationProvider;

    // roles ROOT allow to access /admin/**
    // roles USER allow to access /user/**
    // custom 403 access denied handler
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/home", "/about").permitAll()
                .antMatchers("/admin/**").hasAnyRole("ROOT")
                .antMatchers("/user/**").hasAnyRole("ROOT", "USER")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler);
    }


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        
    	auth.userDetailsService(customerLoginService).passwordEncoder(getPasswordEncoder()); //Using UserDetailService
    	
        // auth.authenticationProvider(customAuthenticationProvider); //Using AauthenticationProvider
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
    	return new BCryptPasswordEncoder(4);
    }

}
