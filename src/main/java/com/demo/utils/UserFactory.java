package com.demo.utils;

import java.util.ArrayList;
import java.util.List;
import com.demo.pojo.UserDTO;

public class UserFactory {
	public static List<UserDTO> getUserList(){
		List<UserDTO> listUsers = new ArrayList<UserDTO>();
		listUsers.add(new UserDTO("root", "123", "ROOT"));
		listUsers.add(new UserDTO("user", "123", "USER"));
		return listUsers;
	}
}
