package com.demo.providers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.demo.services.CustomerLoginService;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private CustomerLoginService customerLoginService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String principal = authentication.getName();
        String credentials = (String)authentication.getCredentials();
        User user = (User)customerLoginService.loadUserByUsername(principal);

        if(user!=null){
          if(passwordEncoder.matches(credentials, user.getPassword())){
                System.out.println("Success!!!");
                return new UsernamePasswordAuthenticationToken(user.getUsername(),
                        user.getPassword(),
                        user.getAuthorities());
            }else{
                System.out.println("Error de login: Clave fallida");
                throw new BadCredentialsException("Error de login: Clave fallida");
            }
        }else{
            System.out.println("Error de login: Clave fallida");
            throw new UsernameNotFoundException("Error de login: No existe este usuario");
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
